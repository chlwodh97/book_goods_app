class BookItem {
  num id;
  String name;
  String imgTitle;
  num price;

  BookItem(this.id, this.imgTitle, this.name, this.price);

  // 제이슨을 받아서 아이템으로 바꿔주는애
  factory BookItem.fromJson(Map<String, dynamic> json){
    return BookItem(
        json['id'],
      json['name'],
      json['imgTitle'],
      json['price'],
    );
  }
}